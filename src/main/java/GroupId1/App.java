package GroupId1;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static spark.Spark.*;





/**
 * Hello world!
 *
 */
public class App 
{

    private static String renderWithMustache(Document doc) throws IOException {
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache mustache = mf.compile("template.mustache");

        Map<String,Object> values= new HashMap<>();
        values.put("name","Gigi");

        StringWriter sw=new StringWriter();
        mustache.execute(sw,values).flush();
        return sw.toString();

    }


    public static void main( String[] args )
    {
    //    System.out.println( "Hello World!!" );
        Spark.get("/*", (Request req, Response res) ->{
            // citeste contentul de la acest url la care ne conectam
            // Document este un thread de obiecte
           // Document doc = Jsoup.connect("http://en.wikipedia.org/").get();

            String requestedArticle= req.splat()[0];

            Document doc=Jsoup.connect("https://en.wikipedia.org/wiki/"+requestedArticle).get();

            // filterul returneaza ce are truue in ( )
            // de exemplu :  .filter(a->a.text() != null )
            // returneaza textul care nu e null


            // outer html = ia tot html-ul , inclusiv tagg-urile
            // set de reguli pentru structurare url- rest
            Elements paragraphs=doc.select("#mw-content-text p");

            // citim paragraful cu indexul 0
            Element firstPar=paragraphs.get(0);

            // retinem textul
           String result= firstPar.text();


            Elements allLinks=doc.select("a");

            // parcurgem elementele si le concatenam
            for(Element link:allLinks)
            {
                result+="<br><a href=\""+ link.attr("href")+"\">"+link.text()+"</a><br>";
            }
            return result;



// metoda 2

            // joining ajuta sa unesti toate streamurile (le contactenezi)
            // trebuie sa ai un collect atunci cand vrei sa apelezi .filter .map si toate astea
     //   String titlesMerged=allLinks.stream().map(a->a.attr("title")).collect(Collectors.joining(", "));

    //        return titlesMerged;



        });
    }




}
